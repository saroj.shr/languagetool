#!/bin/sh

IMAGE_NAME=$CI_REGISTRY/languagetool:$CI_PIPELINE_ID

# buildah login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY

buildah login -u gitlab-ci-token -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

buildah bud --squash -t $IMAGE_NAME .

buildah push $IMAGE_NAME
