#!/bin/sh

IMAGE_NAME=$CI_REGISTRY/languagetool

skopeo login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY

skopeo copy docker://$IMAGE_NAME:$CI_PIPELINE_ID docker://$IMAGE_NAME:latest
